﻿using System;
using System.Activities;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;

// These namespaces are found in the Microsoft.Xrm.Sdk.Workflow.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace LEG.CRM.Workflow
{

    public sealed class GetParentBUOfCurentEntity : CodeActivity
    {
        [RequiredArgument]
        [Input("Regarding Record Guid")]
        public InArgument<string> InArgRegardingRecordGuid { get; set; }

        [Output("Parent BU")]
        [ReferenceTarget("businessunit")]
        public OutArgument<EntityReference> OutParentBU { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext WorkflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(WorkflowContext.InitiatingUserId);
            // Obtain the runtime value of the Text input argument
            var RegardingId = InArgRegardingRecordGuid.Get<string>(context);
            string fetchParentBU = string.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
  <entity name='account'>
    <attribute name='name' />
    <filter type='and'>
      <condition attribute='accountid' operator='eq' uitype='account' value='{0}' />
    </filter>
    <link-entity name='businessunit' from='businessunitid' to='owningbusinessunit' visible='false' link-type='outer' alias='a_6ad8133d2f1e4c43a3da460bacb3d6a5'>
      <attribute name='parentbusinessunitid' alias='a_6ad8133d2f1e4c43a3da460bacb3d6a5_parentbusinessunitid'/>
    </link-entity>
  </entity>
</fetch>",RegardingId);

            EntityCollection resultFetchParentBU = service.RetrieveMultiple(new FetchExpression(fetchParentBU));
            if(resultFetchParentBU != null && resultFetchParentBU.Entities.Count > 0)
            {
                var aliasvalueParentBU = resultFetchParentBU.Entities[0].GetAttributeValue<AliasedValue>("a_6ad8133d2f1e4c43a3da460bacb3d6a5_parentbusinessunitid");
                OutParentBU.Set(context, (EntityReference)aliasvalueParentBU.Value);
            }
        }
    }
}
