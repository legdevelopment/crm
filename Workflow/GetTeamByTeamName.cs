﻿using System;
using System.Activities;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;

// These namespaces are found in the Microsoft.Xrm.Sdk.Workflow.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;


namespace LEG.CRM.Workflow
{

    public sealed class GetTeamByTeamName : CodeActivity
    {
        // Define activity input arguments of type string        
        [RequiredArgument]
        [Input("Access Team Name")]        
        public InArgument<string> AccessTeamName { get; set; }

        [Output("Output Team")]
        [ReferenceTarget("team")]
        public InOutArgument<EntityReference> OutTeam { get; set; }
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext WorkflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(WorkflowContext.InitiatingUserId);
            //  Query using ConditionExpression and FilterExpression
            ConditionExpression condition = new ConditionExpression();
            //attribute name add to condition 
            condition.AttributeName = "name";
            //operator add to condition 
            condition.Operator = ConditionOperator.Equal;
            //values added to condition 
            condition.Values.Add(AccessTeamName.Get<string>(context));
            // filter creation 
            FilterExpression filter = new FilterExpression();
            //condition added 
            filter.Conditions.Add(condition);
            //create query expression 
            QueryExpression query = new QueryExpression("team");
            //filter added to query 
            query.Criteria.AddFilter(filter);
            //retrieve all columns 
            query.ColumnSet = new ColumnSet("name");
            // execute query which will retrieve the Access team teamplate 
            EntityCollection accessTeamColl = service.RetrieveMultiple(query);
            if (accessTeamColl != null && accessTeamColl.Entities.Count > 0)
            {
                OutTeam.Set(context, new EntityReference(accessTeamColl.Entities[0].LogicalName, accessTeamColl[0].Id));
            }            
        }
    }
}
