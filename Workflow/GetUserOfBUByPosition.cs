﻿using System;
using System.Activities;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;

// These namespaces are found in the Microsoft.Xrm.Sdk.Workflow.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk.Workflow;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;

namespace LEG.CRM.Workflow
{

    public sealed class GetUserOfBUByPosition : CodeActivity
    {
        [RequiredArgument]
        [Input("Business Unit")]
        [ReferenceTarget("businessunit")]
        public InArgument<EntityReference> InBusinessUnit { get; set; }

        [RequiredArgument]
        [Input("Position")]
        [ReferenceTarget("position")]
        public InArgument<EntityReference> InPosition { get; set; }

        [Input("In Default")]
        [Output("Out User")]
        [ReferenceTarget("systemuser")]
        public InOutArgument<EntityReference> InOutUser { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            // Obtain the runtime value of the Text input argument
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext WorkflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(WorkflowContext.InitiatingUserId);
            var currBu = InBusinessUnit.Get<EntityReference>(context);
            var currPosition = InPosition.Get<EntityReference>(context);
            string UserByPositionAndBu = string.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
  <entity name='systemuser'>    
    <attribute name='systemuserid'/>    
    <filter type='and'>
      <condition attribute='businessunitid' operator='eq' uitype='businessunit' value='{0}' />
      <condition attribute='positionid' operator='eq' uitype='position' value='{1}' />
    </filter>
  </entity>
</fetch>", currBu.Id, currPosition.Id);

            tracingService.Trace(string.Format("Get {0}:{1} user of {2}:{3}", currPosition.Name, currPosition.Id, currBu.Name,currBu.Id));
            EntityCollection MemberofTeamUsers = service.RetrieveMultiple(new FetchExpression(UserByPositionAndBu));
            if(MemberofTeamUsers.Entities.Count > 0)
            {
                InOutUser.Set(context, new EntityReference(MemberofTeamUsers.Entities[0].LogicalName, MemberofTeamUsers.Entities[0].Id));
            }
        }
    }
}
