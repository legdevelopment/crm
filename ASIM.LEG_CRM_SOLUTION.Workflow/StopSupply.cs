﻿// <copyright file="Class1.cs" company="">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author></author>
// <date>10/21/2014 10:09:49 AM</date>
// <summary>Implements the Class1 Workflow Activity.</summary>
namespace LEG_CRM_SOLUTION.Workflow
{
    using System;
    using System.Activities;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Workflow;

    //<snippetSendEmailUsingTemplate>
    using System.ServiceModel.Description;
    using System.Text;

    // These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
    // found in the SDK\bin folder.
    using Microsoft.Xrm.Sdk.Query;
    using Microsoft.Xrm.Sdk.Discovery;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Client;


    // This namespace is found in Microsoft.Crm.Sdk.Proxy.dll assembly
    // found in the SDK\bin folder.
    using Microsoft.Crm.Sdk.Messages;
    using System.Text.RegularExpressions;
    using System.Globalization;

    public sealed class StopSupply : CodeActivity
    {
        /// <summary>
        /// Executes the workflow activity.
        /// </summary>
        /// <param name="executionContext">The execution context.</param>
        /// 
        private Guid _contactId;
        private Guid _templateId;

        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            string Day = string.Empty;

            DateTime _Date = DateTime.MinValue;

            if (tracingService == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            tracingService.Trace("Entered Class1.Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            // Create the context
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            if (context == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve workflow context.");
            }

            tracingService.Trace("StopSupply.Execute(), Correlation Id: {0}, Initiating User: {1}",
                context.CorrelationId,
                context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            
            try
            {
                // TODO: Implement your custom Workflow business logic.
                // Retrieve the id
                Guid accountId = this.inputEntity.Get(executionContext).Id;

                // Create the 'From:' activity party for the email
                ActivityParty fromParty = new ActivityParty
                {
                    PartyId = new EntityReference(SystemUser.EntityLogicalName, new Guid("6E76364A-DDFF-E311-80C5-0050568703B0"))
                };
                
                // Create the 'To:' activity party for the email
                ActivityParty[] toParties;


                //Retrieve contacts for all the suppliers from the same business group
                string fetchxml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                      <entity name='contact'>
                                        <attribute name='fullname' />
                                        <attribute name='telephone1' />
                                        <attribute name='contactid' />
                                        <attribute name='emailaddress1' />
                                        <order attribute='emailaddress1' descending='false' />
                                        <filter type='and'>
                                          <condition attribute='new_stopsupplycont' operator='eq' value='1' />
                                          <condition attribute='donotemail' operator='eq' value='0' />
                                          <condition attribute='statecode' operator='eq' value='0' />
                                        </filter>
                                        <link-entity name='account' from='accountid' to='parentcustomerid' alias='ab'>
                                          <filter type='and'>
                                            <condition attribute='new_businessgroup' operator='eq' value='" + this.businessGroup.Get(executionContext).Value+ @"' />
                                             <condition attribute='new_membertype' operator='eq' value='100000003' />
                                            <condition attribute='statecode' operator='eq' value='0' />
                                          </filter>
                                        </link-entity>
                                      </entity>
                                    </fetch>";


                EntityCollection result = service.RetrieveMultiple(new FetchExpression(fetchxml)); 

                if (result != null && result.Entities.Count > 0)
                {
                    toParties = new ActivityParty[result.Entities.Count];
                    int i = 0;
                    String previous_email="";
                    foreach (Entity _entity in result.Entities)
                    {


                        if (_entity.GetAttributeValue<string>("emailaddress1") != null && previous_email != _entity.Attributes["emailaddress1"].ToString() && IsValidEmail(_entity.Attributes["emailaddress1"].ToString()))
                        {
                            toParties[i] = new ActivityParty();
                            _contactId = (Guid)_entity.Attributes["contactid"];
                            toParties[i].PartyId = new EntityReference("contact", _contactId);
                            i++;
                            previous_email = _entity.Attributes["emailaddress1"].ToString();
                        }

                        
                        
                    }



                    //toParties[i] = new ActivityParty();
                    ////_contactId = service.Create(emailContact);
                    
                    //toParties[i].PartyId = this.internalGroupEmail.Get(executionContext);
                    //Console.WriteLine("Created a contact: {0}.", emailContact.FirstName + " " + emailContact.LastName);

                    ActivityParty[] mainparty = new ActivityParty[1];
                    mainparty[0] = new ActivityParty();
                    mainparty[0].PartyId = this.internalGroupEmail.Get(executionContext);

                    //// Create an e-mail message.
                    Email email = new Email
                    {
                        To = mainparty,
                        Bcc = toParties,
                        From = new ActivityParty[] { fromParty },
                        Subject = "SDK Sample e-mail",
                        Description = "SDK Sample for SendEmailFromTemplate Message.",
                        DirectionCode = true
                    };
                    //Create a query expression to get one of Email Template of type "contact"

                    QueryExpression queryBuildInTemplates = new QueryExpression
                    {
                        EntityName = "template",
                        ColumnSet = new ColumnSet("templateid", "templatetypecode", "title"),
                        Criteria = new FilterExpression()
                    };
                    queryBuildInTemplates.Criteria.AddCondition("templatetypecode",
                        ConditionOperator.Equal, "account");
                    queryBuildInTemplates.Criteria.AddCondition("title",
                        ConditionOperator.Equal, this.emailTemplate.Get(executionContext));
                    EntityCollection templateEntityCollection = service.RetrieveMultiple(queryBuildInTemplates);

                    if (templateEntityCollection.Entities.Count > 0)
                    {
                        _templateId = (Guid)templateEntityCollection.Entities[0].Attributes["templateid"];
                    }
                    else
                    {
                        throw new ArgumentException("Standard Email Templates are missing");
                    }             

                    //Reading Template
                    SendEmailFromTemplateRequest emailUsingTemplateReq = new SendEmailFromTemplateRequest
                    {
                        Target = email,

                        // TemplateId for StopSupply - Hold Email
                        TemplateId = _templateId,//new Guid("A70B6753-BD58-E411-80DE-0050568703B0"),

                        // The regarding Id is required, and must be of the same type as the Email Template.
                        RegardingId = accountId,
                        RegardingType = "account"
                    };

                    SendEmailFromTemplateResponse emailUsingTemplateResp = (SendEmailFromTemplateResponse)service.Execute(emailUsingTemplateReq);

                   
                        
                }



                //// Create the 'To:' activity party for the email
                //ActivityParty toParty = new ActivityParty
                //{
                //    PartyId = new EntityReference("account", accountId)
                //};



                // Create a task entity
                //Entity task = new Entity();
                //task.LogicalName = "task";
                //task["subject"] = accountId.ToString();
                //task["regardingobjectid"] = new EntityReference("account", accountId);
                //Guid taskId = service.Create(task);
                //this.taskCreated.Set(executionContext,
                //new EntityReference("task", taskId));
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                tracingService.Trace("Exception: {0}", e.ToString());

                // Handle the exception.
                throw;
            }

            tracingService.Trace("Exiting Class1.Execute(), Correlation Id: {0}", context.CorrelationId);
        }


        // Define Input/Output Arguments
        [RequiredArgument]
        [Input("InputEntity")]
        [ReferenceTarget("account")]
        public InArgument<EntityReference> inputEntity { get; set; }

        [RequiredArgument]
        [Input("Business Group")]
        [AttributeTarget("account", "new_businessgroup")]
        public InArgument<OptionSetValue> businessGroup { get; set; }

        [RequiredArgument]
        [Input("InternalGroupEmail")]
        [ReferenceTarget("contact")]
        public InArgument<EntityReference> internalGroupEmail { get; set; }

        [RequiredArgument]
        [Input("Email Template")]
        public InArgument<string> emailTemplate { get; set; }

        [Output("TaskCreated")]
        [ReferenceTarget("task")]
        public OutArgument<EntityReference> taskCreated { get; set; }

        bool invalid = false;

        private bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }

}