﻿using System;
using System.Activities;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;

// These namespaces are found in the Microsoft.Xrm.Sdk.Workflow.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk.Workflow;

namespace LEG.CRM.Workflow
{

    public partial class AutoNumber_GenerateNumberString : CodeActivity
    {
        [Input("Prefix")]                
        public InArgument<string> Prefix { get; set; }

        [Input("Number Length")]        
        public InArgument<int> NumberLength { get; set; }

        [Input("Incremented By")] 
        public InArgument<int> IncrementedBy { get; set; }

        [Input("Last Number")]
        public InArgument<int> LastNumber { get; set; }

        [RequiredArgument]        
        [Input("Auto Number Definition")]
        [ReferenceTarget("new_autonumberdefinition")]
        public InArgument<EntityReference> AutoNumberDefinition { get; set; }

        [Output("New Number")]        
        public OutArgument<string> NewNumber { get; set; }
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            //Create the tracing service
            ITracingService tracingService = context.GetExtension<ITracingService>();

            var nextNumber = LastNumber.Get<int>(context) + IncrementedBy.Get<int>(context);
            var actualNumberLength = NumberLength.Get<int>(context) - Prefix.Get<string>(context).Length;
            if (actualNumberLength < 0) actualNumberLength = 0;

            var newNumberString = Prefix.Get<string>(context) + nextNumber.ToString().PadLeft(actualNumberLength,'0');
            if (actualNumberLength > 0 && newNumberString.Length > NumberLength.Get<int>(context))
            {
                tracingService.Trace(string.Format("Lenght of new number string is greater than {0} characters", NumberLength.Get<int>(context)));
                return;
                //throw new Exception(string.Format("Lenght of new number string is greater than {0} characters", NumberLength.Get<int>(context)));
            }

            //Output
            NewNumber.Set(context, newNumberString);

            //Retrieve the CrmService so that we can retrieve the loan application
            IWorkflowContext WorkflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(WorkflowContext.InitiatingUserId);
            
            //Update Auto number definition
            tracingService.Trace("Update latest number to Auto Number Definition");
            Entity updateAutoNumberDefinition = new Entity(AutoNumberDefinition.Get<EntityReference>(context).LogicalName);
            updateAutoNumberDefinition["new_autonumberdefinitionid"] = AutoNumberDefinition.Get<EntityReference>(context).Id;
            updateAutoNumberDefinition["new_lastnumber"] = nextNumber;
            service.Update(updateAutoNumberDefinition);
        }
    }
}
