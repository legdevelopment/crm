﻿using System;
using System.Activities;

// These namespaces are found in the Microsoft.Xrm.Sdk.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk;

// These namespaces are found in the Microsoft.Xrm.Sdk.Workflow.dll assembly
// located in the SDK\bin folder of the SDK download.
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;

namespace LEG.CRM.Workflow
{

    public sealed class EmailToTeam : CodeActivity
    {
        // Define an activity input argument of type string
        [RequiredArgument]
        [Input("Recieved Team")]
        [ReferenceTarget("team")]
        public InArgument<EntityReference> ReceviedTeam { get; set; }
                
        [Input("Email Template")]
        [ReferenceTarget("template")]
        public InArgument<EntityReference> EmailTempate { get; set; }

        [Input("Email Subject")]        
        public InArgument<string> EmailSubject { get; set; }

        [Input("Email Body")]
        public InArgument<string> EmailBody { get; set; }

        [RequiredArgument]
        [Input("Regarding Record Guid")]
        public InArgument<string> RegardingRecordGuid { get; set; }

        [RequiredArgument]
        [Input("Regarding Record Entity Logical Name")]
        public InArgument<string> RegardingEntityLogicalName { get; set; } 
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext WorkflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(WorkflowContext.InitiatingUserId);
            EntityReference EmailTemplateObject = EmailTempate.Get<EntityReference>(context);

            // Retrieve all team member belonging to selected team              
            string MemberOfTeamfetch = string.Format(@"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
  <entity name='systemuser'>
    <attribute name='systemuserid' />    
    <link-entity name='teammembership' from='systemuserid' to='systemuserid' visible='false' intersect='true'>
      <link-entity name='team' from='teamid' to='teamid' alias='ab'>
        <filter type='and'>
          <condition attribute='teamid' operator='eq' value='{0}' />
        </filter>
      </link-entity>
    </link-entity>
  </entity>
</fetch>",ReceviedTeam.Get<EntityReference>(context).Id);

            tracingService.Trace(string.Format("Get member user of team", ReceviedTeam.Get<EntityReference>(context).Name));
            EntityCollection MemberofTeamUsers = service.RetrieveMultiple(new FetchExpression(MemberOfTeamfetch));
            if (MemberofTeamUsers == null || MemberofTeamUsers.Entities.Count == 0)
                return;
            
            var RegardingObject = new EntityReference(RegardingEntityLogicalName.Get<string>(context), new Guid(RegardingRecordGuid.Get<string>(context)));

            foreach (var item in MemberofTeamUsers.Entities)
            {
                // Create the 'To:' activity party for the email
                var toParty = new Entity("activityparty");
                toParty["partyid"] = new EntityReference(item.LogicalName, item.Id);
                // Create an e-mail message.
                var email = new Entity("email");
                
                email.Attributes["to"] = new Entity[] { toParty };
                //From = new ActivityParty[] { fromParty },
                email.Attributes["subject"] = EmailSubject.Get<string>(context);
                email.Attributes["description"] = EmailBody.Get<string>(context);
                if(EmailTemplateObject == null)
                {
                    var _emailId = service.Create(email);
                    SendEmailRequest sendEmailreq = new SendEmailRequest
                    {
                        EmailId = _emailId,
                        TrackingToken = "",
                        IssueSend = true
                    };

                    SendEmailResponse sendEmailresp = (SendEmailResponse)service.Execute(sendEmailreq);
                }
                else
                {
                    // Create the request
                    SendEmailFromTemplateRequest emailUsingTemplateReq = new SendEmailFromTemplateRequest
                    {
                        Target = email,
                        // Use a built-in Email Template of type "contact".
                        TemplateId = EmailTempate.Get<EntityReference>(context).Id,
                        // The regarding Id is required, and must be of the same type as the Email Template.
                        RegardingId = RegardingObject.Id,
                        RegardingType = RegardingObject.LogicalName
                    };
                    tracingService.Trace(string.Format("Email to {0}", item.Id));
                    SendEmailFromTemplateResponse emailUsingTemplateResp = (SendEmailFromTemplateResponse)service.Execute(emailUsingTemplateReq);
                }
                
            }
        }
    }
}
