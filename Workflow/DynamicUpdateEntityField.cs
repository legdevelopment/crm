﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using Microsoft.Xrm.Sdk.Workflow;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace LEG.CRM.Workflow
{

    public sealed class DynamicUpdateEntityField : CodeActivity
    {

        #region Define activity input argument of type string
        [Input("Money input")]
        public InArgument<Money> MoneyParameter { get; set; }

        [Input("Bool input")]
        public InArgument<bool> BoolParameter { get; set; }

        [Input("DateTime input")]
        public InArgument<DateTime> DateTimeParameter { get; set; }

        [Input("Decimal input")]
        public InArgument<decimal> DecimalParameter { get; set; }

        [Input("Double input")]
        public InArgument<double> DoubleParameter { get; set; }

        [Input("Int input")]
        public InArgument<int> IntParameter { get; set; }

        [Input("String input")]
        public InArgument<string> StringParameter { get; set; }

        
        [RequiredArgument]
        [Input("Regarding Record Guid")]
        public InArgument<string> RegardingRecordGuid { get; set; }

        [RequiredArgument]
        [Input("Regarding Record Entity Logical Name")]
        public InArgument<string> RegardingEntityLogicalName { get; set; }

        [RequiredArgument]
        [Input("Attribute Name")]
        public InArgument<string> AttributeNameParameter { get; set; }
        
        [RequiredArgument]
        [Input("Attribute Type:Bool;DateTime;Decimal;Double;Int;Money;String")]
        public InArgument<string> AttributeTypeParameter { get; set; }

        [Output("Is Changed")]
        [Default("True")]
        public OutArgument<bool> IsChanged { get; set; }

        [Output("Is Sucessful")]
        [Default("True")]
        public OutArgument<bool> IsSucessful { get; set; }

        [Output("Message")]
        public OutArgument<string> Message { get; set; } 
        #endregion
        
        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracingService = context.GetExtension<ITracingService>();
            IWorkflowContext WorkflowContext = context.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = context.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(WorkflowContext.InitiatingUserId);
            // Obtain the runtime value of the input argument
            var AttributeName = AttributeNameParameter.Get<string>(context);
            var AttributeType = AttributeTypeParameter.Get<string>(context);
            var RegardingObjectName = RegardingEntityLogicalName.Get<string>(context);
            var RegardingObjectId = new Guid(RegardingRecordGuid.Get<string>(context));

            var UpdateEntity = new Entity(RegardingObjectName);
            ColumnSet attributes = new ColumnSet(new string[] { AttributeName });

            try
            {
                tracingService.Trace("Retrieve field value of " + AttributeName);
                UpdateEntity = service.Retrieve(RegardingObjectName, RegardingObjectId, attributes);
                switch (AttributeType.ToLower())
                {
                    //Bool
                    case "bool":
                        if (UpdateEntity.GetAttributeValue<bool>(AttributeName) == BoolParameter.Get<bool>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }
                        else
                            UpdateEntity.Attributes[AttributeName] = BoolParameter.Get<bool>(context);
                        break;
                    //DateTime
                    case "datetime":
                        if(UpdateEntity.GetAttributeValue<DateTime>(AttributeName) == DateTimeParameter.Get<DateTime>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }
                        else
                            UpdateEntity.Attributes[AttributeName] = DateTimeParameter.Get<DateTime>(context);
                        break;
                    //Decimal
                    case "decimal":
                        if (UpdateEntity.GetAttributeValue<Decimal>(AttributeName) == DecimalParameter.Get<Decimal>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }          
                        else
                            UpdateEntity.Attributes[AttributeName] = DecimalParameter.Get<Decimal>(context);
                        break;
                    //Double
                    case "double":
                        if (UpdateEntity.GetAttributeValue<Double>(AttributeName) == DoubleParameter.Get<Double>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }            
                        else
                            UpdateEntity.Attributes[AttributeName] = DoubleParameter.Get<Double>(context);
                        break;
                    //Int
                    case "int":
                        if (UpdateEntity.GetAttributeValue<int>(AttributeName) == IntParameter.Get<int>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }       
                        else
                            UpdateEntity.Attributes[AttributeName] = IntParameter.Get<int>(context);
                        break;
                    //Money
                    case "money":
                        if (UpdateEntity.GetAttributeValue<Money>(AttributeName) == MoneyParameter.Get<Money>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }          
                        else
                            UpdateEntity.Attributes[AttributeName] = MoneyParameter.Get<Money>(context);
                        break;
                    //String
                    case "string":
                        if (UpdateEntity.GetAttributeValue<string>(AttributeName) == StringParameter.Get<string>(context))
                        {
                            IsChanged.Set(context, false);
                            return;
                        }
                        else
                            UpdateEntity.Attributes[AttributeName] = StringParameter.Get<string>(context);
                        break;
                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                Message.Set(context, ex.Message);
                IsSucessful.Set(context, false);
                return;
            }
            
            try
            {
                service.Update(UpdateEntity);                
                IsSucessful.Set(context, true);
            }
            catch (Exception ex)
            {
                
               Message.Set(context, ex.Message);
               IsSucessful.Set(context, false);
            }
        }
    }
}
